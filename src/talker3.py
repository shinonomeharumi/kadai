#!/usr/bin/env python
#coding:utf-8

import rospy
from geometry_msgs.msg import Twist # Twist型 ロボットの速度を指令するのによく使われる

import socket
from time import sleep

import re


pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size = 10)
rospy.init_node('tele')

while not rospy.is_shutdown():
	try:
		# Juliusの接続
		HOST = "localhost"# アドレス
		PORT = 10500 # ポート
		cliant = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		cliant.connect((HOST,PORT))

		while 1:
			response = cliant.recv(1024) # 認識結果をresponseとおく
			sleep(0.1) # 全文認識させるためにスリープする
			text = "" # textの初期化
			line_list = response.split('\n')
			for line in line_list:
				if 'WHYPO' in line:
					word = re.compile('WORD="((?!").)+"').search(line)
					text = word.group().split('"')[1] # textに認識した文字が入る

			if text != "":
				vel = Twist()
				if text == '前':	
					vel.linear.x = 2.0
				if text == '後':
					vel.linear.x = -2.0
				if text == '右':
					vel.angular.z = -1.5
				if text == '左':
					vel.angular.z = 1.5

				pub.publish(vel)
				vel.linear.x = 0.0 # 並進速度の初期化
				vel.angular.z = 0.0 # 回転速度の初期化

	except KeyboardInterrupt: # 例外が発生した時の処理
		break


