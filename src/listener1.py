#!/usr/bin/env python
#coding:utf-8

import rospy
from std_msgs.msg import String

def callback(msg):
    text = msg.data
    if text not in fruits.keys(): # もし辞書にキーが存在しないときの処理
        print('答えが見つかりません')
    else:
        answer = fruits[text]
        print answer

    
#--- ここからメイン
fruits = {'りんご':'赤','ぶどう':'紫','バナナ':'黄','みかん':'オレンジ','もも':'ピンク'}

rospy.init_node('listener')

sub = rospy.Subscriber('chatter', String, callback)

rospy.spin()
