#!/usr/bin/env python
#coding:utf-8
#標準入力でGazeboを動かす

import rospy
from geometry_msgs.msg import Twist # ロボットの速度を指令するのによく使われる

rospy.init_node('vel_publisher')
pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size = 10)

while not rospy.is_shutdown():
        vel = Twist()
        direction = raw_input('f: forward, b: background, l: left, r: right > ')

        if 'f' in direction:
                vel.linear.x = 0.5 # vel.linear=「並進速度」
        if 'b' in direction:
                vel.linear.x = -0.5 # vel.linear=「並進速度」
        if 'l' in direction:
                vel.angular.z = 1.0 # vel.angular=「ロボットのz軸周りの回転」(rad/s)
        if 'r' in direction:
                vel.angular.z = -1.0
        if 'q' in direction:
                break

        print vel
        pub.publish(vel)
